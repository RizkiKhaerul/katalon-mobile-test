import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\rizki\\Downloads\\Android-MyDemoAppRN.1.2.0.build-231.apk', true)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Sauce Labs Backpack (2)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Add To Cart (5)'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Sauce Labs Bike Light (2)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Add To Cart (6)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.ImageView (16)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Proceed To Checkout (3)'), 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Rebecca Winter (7)'), 'Rizki Khaerul', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Mandorley 112 (2)'), 'Ujung Berung', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Truro (2)'), 'Bandung', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Cornwall (2)'), 'Jawa Barat', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - 89750 (2)'), '40619', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - United Kingdom (2)'), 'Indonesia', 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - To Payment (3)'), 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Rebecca Winter (8)'), 'Rizki Khaerul', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - 3258 1265 7568 789 (2)'), '3258 1265 7568 789', 
    0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - 0325 (2)'), '03/25', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - 123 (2)'), '123', 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Review Order (2)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Place Order (2)'), 0)

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - Checkout Complete (2)'), 'Checkout Complete')

Mobile.closeApplication()

