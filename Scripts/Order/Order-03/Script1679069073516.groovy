import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\rizki\\Downloads\\Android-MyDemoAppRN.1.2.0.build-231.apk', true)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Sauce Labs Backpack (1)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Add To Cart (4)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.ImageView (15)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Proceed To Checkout (2)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - To Payment (2)'), 0)

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - Please provide your full name'), 'Please provide your full name.')

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - Please provide your address'), 'Please provide your address.')

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - Please provide your city'), 'Please provide your city.')

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - Please provide your zip code'), 'Please provide your zip code.')

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - Please provide your country'), 'Please provide your country.')

Mobile.closeApplication()

