import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\rizki\\Downloads\\Android-MyDemoAppRN.1.2.0.build-231.apk', true)

Mobile.tap(findTestObject('Object Repository/android.widget.ImageView (19)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Sauce Labs Bolt T-Shirt (4)'), 0)

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - Oh no Your cart is empty. Fill it up with swag to complete your purchase (3)'), 
    'Oh no! Your cart is empty. Fill it up with swag to complete your purchase.')

Mobile.tap(findTestObject('Object Repository/android.widget.ImageView (18)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Log Out'), 0)

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - Log Out (1)'), 'Log Out')

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - Are you sure you sure you want to logout (1)'), 
    'Are you sure you sure you want to logout?')

Mobile.tap(findTestObject('Object Repository/android.widget.Button - LOG OUT'), 0)

Mobile.verifyElementText(findTestObject('Object Repository/android.widget.TextView - You are successfully logged out'), 
    'You are successfully logged out.')

Mobile.tap(findTestObject('Object Repository/android.widget.Button - OK'), 0)

Mobile.closeApplication()

